FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=2.3.1

RUN curl -L https://gitweb.torproject.org/pluggable-transports/snowflake.git/snapshot/snowflake-${VERSION}.tar.gz | tar zxf - --strip-components=1 -C /app/code/ && \
    cd proxy && \
    go build && \
    chown -R cloudron:cloudron /app/code

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

